#[rustversion::before(1.32)]
compile_error!("`err-derive` depends on `skeptic`, which requires rustc >= 1.32");

fn main() {
    #[cfg(feature = "skeptic")]
    skeptic::generate_doc_tests(&["README.md"]);
}
